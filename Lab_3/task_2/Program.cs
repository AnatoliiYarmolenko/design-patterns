﻿using task_2;

class Program
{
    static void Main()
    {
        Hero warrior = new Warrior();
        Hero mage = new Mage();
        Hero paladin = new Paladin();

        Console.WriteLine("Warrior without inventory:");
        warrior.Display();

        Console.WriteLine("\nMage with weapon and armor:");
        mage = new Weapon(mage);
        mage = new Armor(mage);
        mage.Display();

        Console.WriteLine("\nPaladin with all items:");
        paladin = new Weapon(paladin);
        paladin = new Armor(paladin);
        paladin = new Artifact(paladin);
        paladin.Display();
    }
}
