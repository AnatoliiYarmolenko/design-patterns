﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_2
{
    public abstract class Hero
    {
        public abstract void Display();
    }

    public class Warrior : Hero
    {
        public override void Display()
        {
            Console.WriteLine("I am a Warrior.");
        }
    }

    public class Mage : Hero
    {
        public override void Display()
        {
            Console.WriteLine("I am a Mage.");
        }
    }

    public class Paladin : Hero
    {
        public override void Display()
        {
            Console.WriteLine("I am a Paladin.");
        }
    }

}
