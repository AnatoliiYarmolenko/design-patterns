﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_2
{
    public class Weapon : HeroDecorator
    {
        public Weapon(Hero hero) : base(hero) { }

        public override void Display()
        {
            base.Display();
            Console.WriteLine("Equipped with a weapon.");
        }
    }

    public class Armor : HeroDecorator
    {
        public Armor(Hero hero) : base(hero) { }

        public override void Display()
        {
            base.Display();
            Console.WriteLine("Wearing armor.");
        }
    }

    public class Artifact : HeroDecorator
    {
        public Artifact(Hero hero) : base(hero) { }

        public override void Display()
        {
            base.Display();
            Console.WriteLine("Possessing an artifact.");
        }
    }
}
