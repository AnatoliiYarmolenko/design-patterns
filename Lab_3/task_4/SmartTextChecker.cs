﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace task_4
{
    public class SmartTextChecker : ITextReader
    {
        private ITextReader _reader;

        public SmartTextChecker(ITextReader reader)
        {
            _reader = reader;
        }

        public string[,] ReadFile(string filePath)
        {
            Console.WriteLine("Opening file...");
            var result = _reader.ReadFile(filePath);
            Console.WriteLine("File read successfully.");
            Console.WriteLine($"Total lines: {result.GetLength(0)}, Total characters: {result.Length}");
            Console.WriteLine("Closing file...");

            return result;
        }
    }

    public class SmartTextReaderLocker : ITextReader
    {
        private ITextReader _reader;
        private Regex _accessPattern;

        public SmartTextReaderLocker(ITextReader reader, string accessPattern)
        {
            _reader = reader;
            _accessPattern = new Regex(accessPattern, RegexOptions.Compiled);
        }

        public string[,] ReadFile(string filePath)
        {
            if (_accessPattern.IsMatch(filePath))
            {
                Console.WriteLine("Access denied!");
                return null;
            }

            return _reader.ReadFile(filePath);
        }
    }

}
