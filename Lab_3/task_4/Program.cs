﻿using System.IO;
using task_4;

class Program
{
    static void Main()
    {
        string currentDirectory = Directory.GetCurrentDirectory();
        string newDirectory = currentDirectory.Replace("\\bin\\Debug\\net6.0", "");
        string filePath = Path.Combine(newDirectory, "test.txt");

        ITextReader reader = new SmartTextReader();
        ITextReader checker = new SmartTextChecker(reader);
        ITextReader locker = new SmartTextReaderLocker(checker, @"restricted.txt$");

        Console.WriteLine("Trying to read 'test.txt'...");
        locker.ReadFile(filePath);

        Console.WriteLine("\nTrying to read 'restricted.txt'...");
        locker.ReadFile(Path.Combine(newDirectory, "restricted.txt"));
    }
}