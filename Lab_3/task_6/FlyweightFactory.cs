﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_6
{
    public class FlyweightFactory
    {
        private readonly Dictionary<string, LightElementNode> _elements = new Dictionary<string, LightElementNode>();

        public LightElementNode GetElement(string tagName, bool isSelfClosing = false)
        {
            string key = tagName + (isSelfClosing ? "/self" : "");
            if (!_elements.ContainsKey(key))
            {
                _elements[key] = new LightElementNode(tagName, isSelfClosing);
            }

            return new LightElementNode(_elements[key].TagName, _elements[key].IsSelfClosing)
            {
                CssClasses = new List<string>(_elements[key].CssClasses),
                Attributes = new Dictionary<string, string>(_elements[key].Attributes)
            };
        }
    }

}
