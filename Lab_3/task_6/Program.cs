﻿using task_6;

public class Program
{
    public static void Main()
    {
        string currentDirectory = Directory.GetCurrentDirectory();
        string newDirectory = currentDirectory.Replace("\\bin\\Debug\\net6.0", "");
        string filePath = Path.Combine(newDirectory, "book.txt");

        List<LightNode> nodes = ParseBookTextToHtml(filePath);

        LightElementNode root = new LightElementNode("div", false);
        foreach (var node in nodes)
        {
            root.AddChild(node);
        }

        Console.WriteLine(root.PrettyPrint());

        GC.Collect();
        long memoryBeforeFlyweight = GC.GetTotalMemory(true);

        FlyweightFactory factory = new FlyweightFactory();
        List<LightNode> flyweightNodes = ParseBookTextToHtmlUsingFlyweight(filePath, factory);

        LightElementNode flyweightRoot = new LightElementNode("div", false);
        foreach (var node in flyweightNodes)
        {
            flyweightRoot.AddChild(node);
        }

        Console.WriteLine(flyweightRoot.PrettyPrint());

        GC.Collect();
        long memoryAfterFlyweight = GC.GetTotalMemory(true);

        Console.WriteLine($"Memory usage before Flyweight: {memoryBeforeFlyweight} bytes");
        Console.WriteLine($"Memory usage after Flyweight: {memoryAfterFlyweight - memoryBeforeFlyweight} bytes");
    }

    public static List<LightNode> ParseBookTextToHtml(string filePath)
    {
        var nodes = new List<LightNode>();
        var lines = File.ReadAllLines(filePath);

        for (int i = 0; i < lines.Length; i++)
        {
            string line = lines[i].TrimEnd();
            if (string.IsNullOrEmpty(line)) continue;

            if (i == 0)
            {
                var h1 = new LightElementNode("h1", false);
                h1.AddChild(new LightTextNode(line));
                nodes.Add(h1);
            }
            else if (line.Length < 20)
            {
                var h2 = new LightElementNode("h2", false);
                h2.AddChild(new LightTextNode(line));
                nodes.Add(h2);
            }
            else if (char.IsWhiteSpace(line[0]))
            {
                var blockquote = new LightElementNode("blockquote", false);
                blockquote.AddChild(new LightTextNode(line));
                nodes.Add(blockquote);
            }
            else
            {
                var p = new LightElementNode("p", false);
                p.AddChild(new LightTextNode(line));
                nodes.Add(p);
            }
        }

        return nodes;
    }

    public static List<LightNode> ParseBookTextToHtmlUsingFlyweight(string filePath, FlyweightFactory factory)
    {
        var nodes = new List<LightNode>();
        var lines = File.ReadAllLines(filePath);

        for (int i = 0; i < lines.Length; i++)
        {
            string line = lines[i].TrimEnd();
            if (string.IsNullOrEmpty(line)) continue;

            if (i == 0)
            {
                LightElementNode h1Node = factory.GetElement("h1");
                h1Node.AddChild(new LightTextNode(line));
                nodes.Add(h1Node);
            }
            else if (line.Length < 20)
            {
                LightElementNode h2Node = factory.GetElement("h2");
                h2Node.AddChild(new LightTextNode(line));
                nodes.Add(h2Node);
            }
            else if (char.IsWhiteSpace(line[0]))
            {
                LightElementNode blockquoteNode = factory.GetElement("blockquote");
                blockquoteNode.AddChild(new LightTextNode(line));
                nodes.Add(blockquoteNode);
            }
            else
            {
                LightElementNode pNode = factory.GetElement("p");
                pNode.AddChild(new LightTextNode(line));
                nodes.Add(pNode);
            }
        }

        return nodes;
    }
}