﻿using task_5;
class Program
{
    static void Main(string[] args)
    {
        LightElementNode table = new LightElementNode("table", false);
        table.AddClass("my-table");

        LightElementNode tr = new LightElementNode("tr", false);

        LightElementNode td1 = new LightElementNode("td", false);
        td1.AddChild(new LightTextNode("Cell 1"));

        LightElementNode td2 = new LightElementNode("td", false);
        td2.AddChild(new LightTextNode("Cell 2"));

        tr.AddChild(td1);
        tr.AddChild(td2);

        table.AddChild(tr);

        LightElementNode img = new LightElementNode("img", true);
        img.AddAttribute("src", "image.jpg");
        img.AddAttribute("alt", "An image");

        Console.WriteLine(table.OuterHTML);
        Console.WriteLine(table.InnerHTML);
        Console.WriteLine(img.OuterHTML);

        Console.WriteLine(table.PrettyPrint());
        Console.WriteLine(img.PrettyPrint());
    }
}