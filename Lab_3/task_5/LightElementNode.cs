﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_5
{
    class LightElementNode : LightNode
    {
        public string TagName { get; set; }
        public bool IsSelfClosing { get; set; }
        public List<string> CssClasses { get; set; }
        public List<LightNode> Children { get; set; }
        public Dictionary<string, string> Attributes { get; set; }

        public LightElementNode(string tagName, bool isSelfClosing)
        {
            TagName = tagName;
            IsSelfClosing = isSelfClosing;
            CssClasses = new List<string>();
            Children = new List<LightNode>();
            Attributes = new Dictionary<string, string>();
        }

        public void AddClass(string className)
        {
            CssClasses.Add(className);
        }

        public void AddChild(LightNode child)
        {
            Children.Add(child);
        }

        public void AddAttribute(string name, string value)
        {
            Attributes[name] = value;
        }

        private string GetAttributes()
        {
            StringBuilder attributes = new StringBuilder();
            if (CssClasses.Count > 0)
            {
                attributes.Append($" class=\"{string.Join(" ", CssClasses)}\"");
            }
            foreach (var attribute in Attributes)
            {
                attributes.Append($" {attribute.Key}=\"{attribute.Value}\"");
            }
            return attributes.ToString();
        }

        public override string OuterHTML
        {
            get
            {
                if (IsSelfClosing)
                {
                    return $"<{TagName}{GetAttributes()} />";
                }
                else
                {
                    return $"<{TagName}{GetAttributes()}>{InnerHTML}</{TagName}>";
                }
            }
        }
        public override string InnerHTML
        {
            get
            {
                StringBuilder innerHTML = new StringBuilder();
                foreach (var child in Children)
                {
                    innerHTML.Append(child.OuterHTML);
                }
                return innerHTML.ToString();
            }
        }

        public override string PrettyPrint(int indentLevel = 0)
        {
            StringBuilder prettyHTML = new StringBuilder();
            string indent = new string(' ', indentLevel * 2);

            if (IsSelfClosing)
            {
                prettyHTML.AppendLine($"{indent}<{TagName}{GetAttributes()} />");
            }
            else
            {
                prettyHTML.AppendLine($"{indent}<{TagName}{GetAttributes()}>");
                foreach (var child in Children)
                {
                    prettyHTML.AppendLine(child.PrettyPrint(indentLevel + 1));
                }
                prettyHTML.AppendLine($"{indent}</{TagName}>");
            }

            return prettyHTML.ToString().TrimEnd();
        }
    }

}
