The following principles are demonstrated:

1. **DRY (Don't Repeat Yourself)**:
   - Common logic is encapsulated within methods and reused.

2. **KISS (Keep It Simple, Stupid)**:
   - Simple and straightforward class design with clear responsibilities.
   - Example: The `Money` class has methods for setting and getting the amount ([lines 19-23](./ConsoleApp/Money.cs#L19-L23)).

3. **SOLID Principles**:
   - **Single Responsibility Principle**: Each class has a single responsibility.
     - Example: The `Money` class only deals with money-related operations ([lines 19-23](./ConsoleApp/Money.cs#L19-L23)).
   - **Liskov Substitution Principle**: Objects of a superclass should be replaceable with objects of subclasses.
     - Example: The `Product` class can be used as a base class for different types of products if needed ([lines 9-40](./ConsoleApp/Product.cs#L9-L40)).
   - **Dependency Inversion Principle**: High-level modules should not depend on low-level modules.
     - Example: The `Reporting` class interacts with the `Warehouse` class through its methods ([lines 11-21](./ConsoleApp/Reporting.cs#L11-L21)).

4. **YAGNI (You Aren't Gonna Need It)**:
   - The code includes only the necessary functionality required to demonstrate the principles.
   - Example: No additional, unnecessary methods or properties are added to the classes.

5. **Composition Over Inheritance**:
   - The `Warehouse` class contains a list of `WarehouseItem` objects instead of inheriting from a base class.
   - Example: The composition of `WarehouseItem` within `Warehouse` class ([lines 11-16](./ConsoleApp/Warehouse.cs#L11-L16)).

6. **Fail Fast**:
   - The code throws exceptions as soon as an error condition is encountered.
   - Example: The `RemoveQuantity` method in `WarehouseItem` class throws an `InvalidOperationException` if the quantity to be removed exceeds available quantity ([lines 30-33](./ConsoleApp/WarehouseItem.cs#L30-L33)).
