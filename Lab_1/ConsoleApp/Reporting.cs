﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp 
{
    public class Reporting
    {
        public void RegisterDelivery(Warehouse warehouse, Product product, DateTime lastDeliveryDate, int quantity)
        {
            warehouse.AddProduct(product, lastDeliveryDate, quantity);
            Console.WriteLine($"Delivery registered: {product.Name} ({quantity})");
        }

        public void RegisterShipment(Warehouse warehouse, string productName, int quantity)
        {
            warehouse.RegisterShipment(productName, quantity);
            Console.WriteLine($"Shipment registered: {quantity} of {productName}");
        }

        public void InventoryReport(Warehouse warehouse)
        {
            Console.WriteLine("Inventory Report:");
            Console.WriteLine(warehouse.ToString());
        }
    }
}
