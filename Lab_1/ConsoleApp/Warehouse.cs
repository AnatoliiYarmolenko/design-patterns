﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
public class Warehouse
{
    private List<WarehouseItem> Items;

    public Warehouse()
    {
            Items = new List<WarehouseItem>();
    }

    public void AddProduct(Product product, DateTime lastDeliveryDate, int quantity)
    {
        var item = Items.FirstOrDefault(i => i.Product.Name == product.Name);
        if (item != null)
        {
            item.AddQuantity(quantity);
        }
        else
        {
                Items.Add(new WarehouseItem(product, lastDeliveryDate, quantity));
        }
    }

    public void RegisterShipment(string productName, int quantity)
    {
        var item = Items.FirstOrDefault(i => i.Product.Name == productName);
        if (item == null)
        {
            throw new InvalidOperationException("No product found.");
        }

        item.RemoveQuantity(quantity);
    }

    public int CountProduct(string productName)
    {
        return Items.Where(i => i.Product.Name == productName).Sum(i => i.Quantity);
    }

    public override string ToString()
    {
        string warehouseContent = "Warehouse Content\n";
        foreach (var item in Items)
        {
            warehouseContent += $"{item}\n";
        }
        return warehouseContent;
    }
}

}
