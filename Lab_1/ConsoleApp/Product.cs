﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class Product
    {
        public string Name { get; private set; }
        public Money Price { get; private set; }
        public string Unit { get; private set; }

        public Product(string name, Money price, string unit)
        {
            Name = name;
            Price = price;
            Unit = unit;
        }

        public void DecreasePrice(int wholePart, int fractionalPart)
        {
            int totalFractionalPart = Price.FractionalPart - fractionalPart;
            int totalWholePart = Price.WholePart - wholePart;

            if (totalFractionalPart < 0)
            {
                totalWholePart -= 1;
                totalFractionalPart += 100;
            }

            Price.SetAmount(totalWholePart, totalFractionalPart);
        }

        public override string ToString()
        {
            return $"{Name} - {Price}";
        }
    }
}
