﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class WarehouseItem
    {
        public Product Product { get; private set; }
        public DateTime LastDeliveryDate { get; private set; }
        public int Quantity { get; private set; }

        public WarehouseItem(Product product, DateTime lastDeliveryDate, int quantity)
        {
            Product = product;
            LastDeliveryDate = lastDeliveryDate;
            Quantity = quantity;
        }

        public void AddQuantity(int quantity)
        {
            Quantity += quantity;
            LastDeliveryDate = DateTime.Now;
        }

        public void RemoveQuantity(int quantity)
        {
            if (quantity > Quantity)
            {
                throw new InvalidOperationException("Not enough stock.");
            }
            Quantity -= quantity;
        }

        public override string ToString()
        {
            return $"{Product} - Unit: {Product.Unit}, Quantity: {Quantity}, Last Delivery: {LastDeliveryDate}";
        }
    }

}
