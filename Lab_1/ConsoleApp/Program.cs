﻿using ConsoleApp;

class Program
{
    static void Main()
    {
        var money = new Money(50, 75);
        var product1 = new Product("Laptop", money, "unit1");
        var product2 = new Product("Phone", new Money(30, 50), "unit2");
        
        var warehouse = new Warehouse();
        var reporting = new Reporting();

        reporting.RegisterDelivery(warehouse, product1, "piece", DateTime.Today, 10);
        reporting.RegisterDelivery(warehouse, product2, "piece", DateTime.Now, 5);
        reporting.InventoryReport(warehouse);


        reporting.RegisterDelivery(warehouse, product1, "piece", DateTime.Now, 20);
        reporting.InventoryReport(warehouse);

        reporting.RegisterShipment(warehouse, "Laptop", "piece", 5);
        reporting.InventoryReport(warehouse);

        Console.WriteLine($"Count of Laptops: {warehouse.CountProduct("Laptop")}");
        Console.WriteLine($"Count of Phones: {warehouse.CountProduct("Phone")}");
    }
}
