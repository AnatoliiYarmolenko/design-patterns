﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class Money
    {
        public int WholePart { get; private set; }
        public int FractionalPart { get; private set; }

        public Money(int wholePart, int fractionalPart)
        {
            SetAmount(wholePart, fractionalPart);
        }

        public void SetAmount(int wholePart, int fractionalPart)
        {
            WholePart = wholePart;
            FractionalPart = fractionalPart;
        }
        public override string ToString()
        {
            return $"{WholePart}.{FractionalPart:D2}";
        }
    }
}
