﻿using task_5;

class Program
{
    static void Main()
    {
        TextDocument doc = new TextDocument("Початковий текст");

        TextEditor editor = new TextEditor(doc);

        editor.Print(); 
        editor.Save(); 
        doc.Content = "Змінений текст";

        editor.Print(); 
        editor.Undo(); 

        editor.Print(); 
    }
}
