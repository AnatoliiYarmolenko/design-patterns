﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_5
{
    class DocumentMemento
    {
        public string Content { get; }

        public DocumentMemento(string content)
        {
            Content = content;
        }
    }
}
