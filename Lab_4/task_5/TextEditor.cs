﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_5
{
     class TextEditor
    {
        private TextDocument _document;
        private Stack<DocumentMemento> _history = new Stack<DocumentMemento>();

        public TextEditor(TextDocument document)
        {
            _document = document;
        }

        public void Save()
        {
            _history.Push(new DocumentMemento(_document.Content));
        }

        public void Undo()
        {
            if (_history.Count > 0)
            {
                var memento = _history.Pop();
                _document.Content = memento.Content;
            }
            else
            {
                Console.WriteLine("Немає можливості відмінити дії.");
            }
        }

        public void Print()
        {
            Console.WriteLine("Поточний зміст документа:");
            Console.WriteLine(_document.Content);
        }

    }
}
