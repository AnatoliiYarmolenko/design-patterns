﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task_4;

    public class NetworkImageLoader : IImageLoaderStrategy
    {
        public async void LoadImage(string href)
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    Console.WriteLine($"Loading image from network: {href}");
                    var response = await client.GetAsync(href);
                    response.EnsureSuccessStatusCode();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Failed to load image from network: {ex.Message}");
                }
            }
        }
    }


