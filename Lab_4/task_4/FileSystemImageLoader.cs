﻿using System;
using System.Net.Http;
using System.IO;
using task_4;

public class FileSystemImageLoader : IImageLoaderStrategy
{
    public void LoadImage(string href)
    {
        if (File.Exists(href))
        {
            Console.WriteLine($"Loading image from file system: {href}");
        }
        else
        {
            Console.WriteLine("File does not exist.");
        }
    }
}

