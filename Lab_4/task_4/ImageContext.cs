﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task_4;

    public class ImageContext
    {
        private IImageLoaderStrategy _imageLoaderStrategy;

        public void SetStrategy(IImageLoaderStrategy strategy)
        {
            _imageLoaderStrategy = strategy;
        }

        public void LoadImage(string href)
        {
            _imageLoaderStrategy.LoadImage(href);
        }
    }


