﻿using System;

class Program
{
    static void Main(string[] args)
    {
        ImageContext imageContext = new ImageContext();

        imageContext.SetStrategy(new FileSystemImageLoader());
        imageContext.LoadImage("path_to_local_image.jpg");

        imageContext.SetStrategy(new NetworkImageLoader());
        imageContext.LoadImage("https://example.com/image.jpg");
    }
}