﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_1
{
    class LevelOneSupport : SupportHandler
    {
        public override void HandleRequest(int level)
        {
            if (level == 1)
            {
                Console.WriteLine("Level One Support: Handling request.");
            }
            else if (_nextHandler != null)
            {
                _nextHandler.HandleRequest(level);
            }
        }
    }

    class LevelTwoSupport : SupportHandler
    {
        public override void HandleRequest(int level)
        {
            if (level == 2)
            {
                Console.WriteLine("Level Two Support: Handling request.");
            }
            else if (_nextHandler != null)
            {
                _nextHandler.HandleRequest(level);
            }
        }
    }

    class LevelThreeSupport : SupportHandler
    {
        public override void HandleRequest(int level)
        {
            if (level == 3)
            {
                Console.WriteLine("Level Three Support: Handling request.");
            }
            else if (_nextHandler != null)
            {
                _nextHandler.HandleRequest(level);
            }
        }
    }

    class LevelFourSupport : SupportHandler
    {
        public override void HandleRequest(int level)
        {
            if (level == 4)
            {
                Console.WriteLine("Level Four Support: Handling request.");
            }
            else if (_nextHandler != null)
            {
                _nextHandler.HandleRequest(level);
            }
            else
            {
                Console.WriteLine("No support available for this level.");
            }
        }
    }


}
