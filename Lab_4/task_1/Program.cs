﻿using task_1;
    class Program
    {
        static void Main(string[] args)
        {
            SupportHandler levelOne = new LevelOneSupport();
            SupportHandler levelTwo = new LevelTwoSupport();
            SupportHandler levelThree = new LevelThreeSupport();
            SupportHandler levelFour = new LevelFourSupport();

            levelOne.SetNextHandler(levelTwo);
            levelTwo.SetNextHandler(levelThree);
            levelThree.SetNextHandler(levelFour);

            while (true)
            {   
                Console.WriteLine("Enter the support level you need (1-4): ");
                if (int.TryParse(Console.ReadLine(), out int level))
                {
                    levelOne.HandleRequest(level);
                }
                else 
                {
                    Console.WriteLine("Invalid input. Please enter a number between 1 and 4.");
                }
            }
        }


    }
