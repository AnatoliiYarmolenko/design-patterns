﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_3
{
    interface IEventListener
    {
        void Update(string eventType, LightElementNode element);
    }
}
