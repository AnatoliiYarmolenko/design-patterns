﻿using task_3;

var div = new LightElementNode("div", false);
var button = new LightElementNode("button", false);
button.AddChild(new LightTextNode("Click Me"));

div.AddChild(button);

var clickListener = new EventListener("ClickListener");
var mouseOverListener = new EventListener("MouseOverListener");

button.AddEventListener("click", clickListener);
button.AddEventListener("mouseover", mouseOverListener);

button.SimulateEvent("click");
button.SimulateEvent("mouseover");

Console.WriteLine(div.PrettyPrint());
