﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_3
{
    class EventListener : IEventListener
    {
        private string name;

        public EventListener(string name)
        {
            this.name = name;
        }

        public void Update(string eventType, LightElementNode element)
        {
            Console.WriteLine($"{name} received {eventType} event from <{element.TagName}> element.");
        }
    }
}
