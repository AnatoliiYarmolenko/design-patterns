﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_3
{
    interface IEventPublisher
    {
        void AddEventListener(string eventType, IEventListener listener);
        void RemoveEventListener(string eventType, IEventListener listener);
        void NotifyEventListeners(string eventType);
    }
}
