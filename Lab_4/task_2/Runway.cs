﻿using System;

class Runway
{
    public readonly Guid Id = Guid.NewGuid();
    private bool _isBusy;

    public bool IsBusy => _isBusy;

    public void MarkAsBusy()
    {
        _isBusy = true;
        HighLightRed();
    }

    public void MarkAsFree()
    {
        _isBusy = false;
        HighLightGreen();
    }

    public void HighLightRed()
    {
        Console.WriteLine($"Runway {this.Id} is busy!");
    }

    public void HighLightGreen()
    {
        Console.WriteLine($"Runway {this.Id} is free!");
    }
}
