﻿using System;
using System.Collections.Generic;

class CommandCentre
{
    private List<Runway> _runways = new List<Runway>();
    private Dictionary<Aircraft, Runway> _landingAssignments = new Dictionary<Aircraft, Runway>();

    public CommandCentre(Runway[] runways)
    {
        this._runways.AddRange(runways);
    }

    public void LandAircraft(Aircraft aircraft)
    {
        foreach (var runway in _runways)
        {
            if (!runway.IsBusy)
            {
                Console.WriteLine($"CommandCentre: Landing {aircraft.Name} on runway {runway.Id}");
                runway.MarkAsBusy();
                _landingAssignments[aircraft] = runway;
                return;
            }
        }
        Console.WriteLine($"CommandCentre: No available runway for {aircraft.Name} to land.");
    }

    public void TakeOffAircraft(Aircraft aircraft)
    {
        if (_landingAssignments.TryGetValue(aircraft, out Runway? runway))
        {
            Console.WriteLine($"CommandCentre: Aircraft {aircraft.Name} is taking off from runway {runway.Id}");
            runway.MarkAsFree();
            _landingAssignments.Remove(aircraft);
        }
        else
        {
            Console.WriteLine($"CommandCentre: Aircraft {aircraft.Name} is not assigned to any runway.");
        }
    }
}
