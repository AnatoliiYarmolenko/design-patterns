﻿using System;

class Program
{
    static void Main()
    {
        Runway[] runways = { new Runway(), new Runway() };
        CommandCentre commandCentre = new CommandCentre(runways);

        Aircraft aircraft1 = new Aircraft("Aircraft 1");
        Aircraft aircraft2 = new Aircraft("Aircraft 2");

        aircraft1.RequestLand(commandCentre);
        aircraft2.RequestLand(commandCentre);

        aircraft1.RequestTakeOff(commandCentre);
        aircraft2.RequestLand(commandCentre);
    }
}
