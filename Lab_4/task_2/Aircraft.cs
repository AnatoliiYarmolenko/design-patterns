﻿using System;

class Aircraft
{
    public string Name;
    public bool IsTakingOff { get; set; }

    public Aircraft(string name)
    {
        this.Name = name;
    }

    public void RequestLand(CommandCentre commandCentre)
    {
        Console.WriteLine($"Aircraft {this.Name} is requesting to land.");
        commandCentre.LandAircraft(this);
    }

    public void RequestTakeOff(CommandCentre commandCentre)
    {
        Console.WriteLine($"Aircraft {this.Name} is requesting to take off.");
        commandCentre.TakeOffAircraft(this);
    }
}
